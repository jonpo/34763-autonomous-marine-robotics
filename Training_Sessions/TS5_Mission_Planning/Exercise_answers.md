# Exercise 1
**Briefing**
During an eelgrass survey that was conducted along the South-East coast of Saltholm in September 2020, traces of netting were spotted within the survey video indicating the presence of ghost nets (lost fishing gear). A subsequent survey is planned for June 2021 to survey the area for ghost nets using a side-scan sonar package designed for use on the BlueROV2. Analysis of the video and trajectory data from the 2020 survey show that a ghost net might be located in the vicinity of [55.603036°, 12.8089368°]. Approximate depth in this location is 6 m.

**Requirements:**
Ghost nets usually have metal components which show up quite strongly on side scan sonar. The components are usually quite small, (e.g. cable diameters < 3 cm). The ghost net expert at DTU Aqua recommends a resolution of approximately 1 to 3 pings / cm. A minimum nadir blindzoneof 2 m is required with a row overlap percentage of 20%.
 
**Equipment:** The BlueROV2 ROV has been equipped with a prototype side scan sonar system as well as a forward facing camera with HFOV=86° (see [bluerov2_description/robots/altimeter_sss.xacro](https://gitlab.gbar.dtu.dk/dtu-asl/courses/34763-autonomous-marine-robotics/-/blob/main/ros_ws/src/bluerov2/bluerov2_description/robots/altimeter_sss.xacro) for the Gazebo Configuration). The beam axis of the side-scan sonar is mounted on each side of the ROV at 45 degrees to the vertical, with a 50° vertical beam width. The range of the system is calibrated for 40 m with a corresponding ping rate of 18 Hz. Due to the range gate blanking interval, the minimum range at this calibration is 1 m. The ROV has 150 m of tether to use. The ROV has a hotel load of 15 watts, it has a thruster draw load of approximately 480 W per kt of forward speed, and a total battery capacity of 266.4 Wh. Five batteries total have been acquired for the mission with near 100% capacity.

**Mission Parameters**
1. Should the survey be conducted at a constant depth or at a constant altitude?

    __Answer:__ The survey should be conducted at a constant altitude. This is because the side-scan sonar system is calibrated for a specific range and the altitude will be used to control the range of the sonar system.

2. What speed should the platform conduct the survey at?

    __Answer:__ The speed of the platform should be selected to ensure that the ROV has enough power to complete the survey. The speed should be selected such that the ROV can complete the survey within the time constraints of the mission. Furthermore we should ensure to follow the DTU Aqua recommendation of 1 to 3 pings / cm, which will be used to calculate the max speed of the ROV.

    We have the pingrate H=18 Hz, and assuming 1 ping/cm we have a speed of 18 cm/s. If we assume 3 pings/cm we have a speed of 54 cm/s.

3. What depth/altitude should the survey be performed at?
![alt text](illustration.png)

    $$
    \tan(\alpha) = \frac{1}{A} \leftrightarrow A = \frac{1}{\tan(\alpha)}
    $$
    which we can figure out based on the drawing. We have $\alpha = 20°$ and we can calculate A as follows:
    $$
    A = \frac{1}{\tan(20)} = 2.75 m
    $$

4. What are the maximum operational limits of the ROV in m, assuming a fixed tether point at depth 0 m?

    5. What is the maximum endurance of ROV for your given speed selection?
    - Max speed: 0.33 knots
    - Power draw at max speed: 480 W
    - Total battery capacity: 266.4 Wh
    - Hotel load: 15 W

6. How long should the transects be?
    - 20 m for turning a 149 m tethered ROV

7. What should the row spacing be?
    - Remember to subtract the blindzone away

8. Is cross-hatching required for this survey?
    - Side scan sonar is directional, therfore the image of the bottom will apear differently if the transect heading is changed.
    - Best practice is to cross hatch (I.e. transect at 90° to the previous transect)
    - Thereby getting two shots of the same area from different angles.

**Mission Constraints:**
You have been allocated 6 hours to survey the area, how many grid searches could 
you do?
Path total length = endurance * survey speed = 1.5 hors * 0.33 knots = 0.495 nm = 0.916 km

For 1 cross hatch survey:
- Tansect length = 130 m (20 m slack for turning)
- Row spacing = 6 m
